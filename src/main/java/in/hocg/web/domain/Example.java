package in.hocg.web.domain;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import in.hocg.web.domain.enums.ExampleType;
import in.hocg.web.utils.support.DeletedModel;
import in.hocg.web.utils.support.SuperModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.sql.Timestamp;

@Data
@ToString
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
@TableName("t_examples")
public class Example extends DeletedModel<Example> {
  @TableField(value = "name")
  private String name;
  @TableField(value = "type")
  private ExampleType type;
}
