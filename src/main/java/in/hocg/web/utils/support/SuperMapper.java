package in.hocg.web.utils.support;

import com.baomidou.mybatisplus.mapper.BaseMapper;

public interface SuperMapper<T> extends BaseMapper<T> {
}
