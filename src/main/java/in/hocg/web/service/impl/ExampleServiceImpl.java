package in.hocg.web.service.impl;

import in.hocg.web.domain.Example;
import in.hocg.web.mapper.ExampleMapper;
import in.hocg.web.service.ExampleService;
import in.hocg.web.utils.support.BaseService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author hocgin
 * @since 2018-06-11
 */
@Service
public class ExampleServiceImpl extends BaseService<ExampleMapper, Example>
  implements ExampleService {

}
