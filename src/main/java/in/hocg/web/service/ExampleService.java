package in.hocg.web.service;


import com.baomidou.mybatisplus.service.IService;
import in.hocg.web.domain.Example;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author hocgin
 * @since 2018-06-11
 */
public interface ExampleService extends IService<Example> {

}
