package in.hocg.web.controller.admin;

import in.hocg.web.utils.support.BaseController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by hocgin on 2018/6/12.
 * email: hocgin@gmail.com
 */
@Controller("adminController")
@RequestMapping("/admin/")
public class IndexController extends BaseController {
    
    @Override
    protected String page(String page) {
        return String.format("/pages/admin/%s", page);
    }
}
